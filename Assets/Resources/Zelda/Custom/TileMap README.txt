NULL -> Undefined. Expect undefined behavior.
WALL -> Simple wall. Player and enemies cannot move through these, projectiles can.
NONE -> Walkable tile (i.e floor). Units and objects can move and spawn freely on these.
DRDL -> Doorway, down-left. This has a half-collider for the wall part and half-empty for the walkable area.
DRDR -> Doorway, down-right. This has a half-collider for the wall part and half-empty for the walkable area.
DRUL -> Doorway, up-left. This has a half-collider for the wall part and half-empty for the walkable area.
DRUR -> Doorway, up-right. This has a half-collider for the wall part and half-empty for the walkable area.
DRWY -> Horizontal doorway. This is walkable area.
LOCK -> A locked door. this is used for both vertical and horizontal locked doors.
BARR -> Barrier. Can only be opened by solving a puzzle.