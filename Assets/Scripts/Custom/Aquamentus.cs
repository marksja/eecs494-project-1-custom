﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class Aquamentus : MonoBehaviour {

	public float speed;
	public float attack_delay;
	public GameObject fireball;
	public GameObject explosion;
	public Vector3 spawn_offset;
	public Vector3 base_target;
	public Vector3 up_offset;
	public bool forward;
	bool isAttacking;
	float time;

	GameObject player;
	Animator anim;
	Health hp;

	// Use this for initialization
	void Start () {
		time = 0;
		forward = true;
		isAttacking = false;
		player = GameObject.FindGameObjectWithTag("Player");
		anim = GetComponent<Animator>();
		hp = GetComponent<Health>();

	}
	
	// Update is called once per frame
	void Update () {
		if(hp.current_health == 0){
			Destroy(gameObject);
		}
		if(time > attack_delay){
			anim.SetBool("isAttacking", true);
			StartCoroutine(AttackingDelay());
			base_target = Vector3.Normalize(player.transform.position - transform.position) * 30;
			time = 0;
			GameController.audioManager.Play("Candle");
			GameObject fire_temp = Instantiate(fireball, transform.position + spawn_offset, Quaternion.identity);
			fire_temp.GetComponent<Fireball>().SetTarget(transform.position + base_target + up_offset);
			fire_temp = Instantiate(fireball, transform.position + spawn_offset, Quaternion.identity);
			fire_temp.GetComponent<Fireball>().SetTarget(transform.position + base_target);
			fire_temp = Instantiate(fireball, transform.position + spawn_offset, Quaternion.identity);
			fire_temp.GetComponent<Fireball>().SetTarget(transform.position + base_target - up_offset);
		}

		//Move back and forth
		transform.Translate(((forward) ? Vector3.left : Vector3.right) * speed * Time.deltaTime);
		if(!CameraBounds.Instance.isWithinBounds(transform.position + Vector3.right)){
			transform.Translate(((forward) ? Vector3.right : Vector3.left) * speed * Time.deltaTime);
			forward = !forward;
		}
		if(Random.Range(0f, 1f) > .98f){
			forward = !forward;
		}		

		time += Time.deltaTime;
	}

	IEnumerator AttackingDelay(){
		yield return new WaitForSeconds(.3f);
		anim.SetBool("isAttacking", false);
	}

	void OnDestroy()
	{
		foreach(GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")){
			Destroy(enemy);
		}
		Instantiate(explosion, transform.position, Quaternion.identity);
	}
}
}