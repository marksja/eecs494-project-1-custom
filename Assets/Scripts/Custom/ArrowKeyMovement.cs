﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class ArrowKeyMovement : MonoBehaviour
{
	[System.Serializable]
	public class MovementProbes
	{
		public MovementProbe left;
        public MovementProbe right;
        public MovementProbe up;
        public MovementProbe down;
	}

	public MovementProbes movementProbes;
	public float movementSpeed = 4;

	public bool ControlsEnabled { get { return controlsEnabled; } }
	
	Animator anim;
	bool controlsEnabled = true;

	public PlayerScript.Direction last_direction;

	void Start()
	{
		anim = GetComponent<Animator>();

		movementProbes.left.onWallTouch.AddListener(SnapHorizontal);
		movementProbes.right.onWallTouch.AddListener(SnapHorizontal);
		movementProbes.up.onWallTouch.AddListener(SnapVertical);
		movementProbes.down.onWallTouch.AddListener(SnapVertical);
	}
	
	void Update()
	{
		if (controlsEnabled) {
			Vector2 input = GetInput();
			Move(input);
		}
	}

	// Move the player in the direction given by input. If the player is not aligned to the grid, this
	// will instead move them within the nearest axis that matches their intended direction.
	// Returns the actual movement vector that was used, for calculations.
	public Vector2 Move(Vector2 dir)
    {
		// Check movement constraints
		Vector2 prewallDir = DiagonalMovementCheck(dir);
		dir = WallCheck(prewallDir);

        // If there is no input, don't bother calculating movement.
        if (dir.magnitude == 0f) {
			// But we still need to animate if we are running into a wall
			if (prewallDir.magnitude != 0f) {
				anim.enabled = true;
				last_direction = DetermineDirection(prewallDir);
				anim.Play(AnimNameFromDirection(last_direction));
			}
			else {
                anim.enabled = false;
			}
            return Vector2.zero;
        }

        float moveAmt = movementSpeed * Time.deltaTime;
        // If we are not aligned properly, need to align ourselves to the grid
        if (!((dir.x == 0f && InVertGrid()) || (dir.y == 0f && InHorizGrid()))) {
            dir = AlignToGrid(dir, ref moveAmt);
        }

		transform.Translate(dir * moveAmt);
        last_direction = DetermineDirection(dir);
		anim.enabled = true;
		anim.Play(AnimNameFromDirection(last_direction));

		return (dir * moveAmt);
    }

	public void DisableControls()
	{
		controlsEnabled = false;
		anim.enabled = false;
	}

	public void EnableControls()
	{
		controlsEnabled = true;
		anim.enabled = true;
	}

	// Returns an input vector based on player controls
	Vector2 GetInput()
	{
		float horizontal = Input.GetAxisRaw("Horizontal");
		float vertical = Input.GetAxisRaw("Vertical");

		return new Vector2(horizontal, vertical);
	}

	// Limits the input vector to one axis. Vertical is prioritized unless we are blocked by a wall
	Vector2 DiagonalMovementCheck(Vector2 input)
	{
		if ((input.y > 0f && !movementProbes.up.Touching) ||
            (input.y < 0f && !movementProbes.down.Touching))
        {
            input.x = 0f;
        }
        else if (input.x != 0f) {
            input.y = 0f;
        }
		return input;
	}

	// Stops the player from moving through walls!
	Vector2 WallCheck(Vector2 input)
	{
		// Vertical walls
		if (InVertGrid() &&
            ((input.y > 0f && movementProbes.up.Touching) ||
             (input.y < 0f && movementProbes.down.Touching)))
        {
            input.y = 0f;
        }

		// Horizontal walls
        if (InHorizGrid() &&
            ((input.x > 0f && movementProbes.right.Touching) ||
             (input.x < 0f && movementProbes.left.Touching)))
        {
            input.x = 0f;
        }

		return input;
	}

	// Sets moveAmt and returns a new Vector which, when used, will align the player properly within
	// their target grid axis
	Vector2 AlignToGrid(Vector2 input, ref float moveAmt)
	{
		float delta = 0f;
		Vector2 dir = Vector2.zero;

		// Vertical movement takes precedence
		if (input.y != 0f) {
			// Align to vertical grid
			delta = transform.position.x % 0.5f;
			dir = Vector2.right;
		}
		else {
			// Align to horizontal grid
			delta = transform.position.y % 0.5f;
			dir = Vector2.up;
		}

		// Proper direction
		if (delta < 0.25f) {
			dir *= -1;
		}

		// Make sure we dont go too far
		if (delta + moveAmt > 0.5f) {
			moveAmt = 0.5f - delta;
		}
		else if (delta - moveAmt < 0f) {
			moveAmt = delta;
		}

		return dir;
	}

	// Returns true if the player is approximately aligned with the horizontal axis
	bool InHorizGrid()
	{
		float vertOffset = transform.position.y % 0.5f;
		return Mathf.Approximately(vertOffset, 0f);
	}

	// Returns true if the player is approximately aligned with the vertical axis
	bool InVertGrid()
	{
		float horizOffset = transform.position.x % 0.5f;
		return Mathf.Approximately(horizOffset, 0f);
	}

	// Snaps the player to the nearest half-grid space in the vertical axis
	public void SnapVertical()
	{
		float posY = Mathf.Round(transform.position.y / 0.5f) * 0.5f;
        transform.position = new Vector2(transform.position.x, posY);
	}

    // Snaps the player to the nearest half-grid space in the horizontal axis
    public void SnapHorizontal()
	{
		float posX = Mathf.Round(transform.position.x / 0.5f) * 0.5f;
        transform.position = new Vector2(posX, transform.position.y);
	}

	PlayerScript.Direction DetermineDirection(Vector2 dir)
	{
		if(dir.x > 0){
			return PlayerScript.Direction.Right;
		}
		else if(dir.x < 0){
			return PlayerScript.Direction.Left;
		}
		else if(dir.y < 0){
			return PlayerScript.Direction.Down;
		}
		else if(dir.y > 0){
			return PlayerScript.Direction.Up;
		}
		else{
			return GetComponent<PlayerScript>().facing_direction;
		}
	}

	string AnimNameFromDirection(PlayerScript.Direction dir)
	{
		switch (dir) {
			case PlayerScript.Direction.Down:
				return "MoveDown";
			case PlayerScript.Direction.Left:
				return "MoveLeft";
			case PlayerScript.Direction.Right:
				return "MoveRight";
			case PlayerScript.Direction.Up:
				return "MoveUp";
		}
		throw new UnityException("No case for direction in ArrowKeyMovement.AnimNameFromDirection!");
	}
}
}