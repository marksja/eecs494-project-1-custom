﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class AudioManager : MonoBehaviour
{
	List<AudioClip> soundEffects;
	AudioSource bgmSource;
	AudioSource soundEffectSource;

	void Start()
	{
		soundEffects = new List<AudioClip>();
		bgmSource = Camera.main.GetComponent<AudioSource>();
		soundEffectSource = Camera.main.gameObject.AddComponent<AudioSource>();
		Object[] clips = Resources.LoadAll("Zelda/Custom/Audio", typeof(AudioClip));
		foreach (AudioClip clip in clips) {
			soundEffects.Add(clip);
		}
	}

	public void Play(string effectName)
	{
		AudioClip clip = soundEffects.Find(x => x.name == effectName);
		if (clip == null) {
			throw new UnityException("AudioClip '" + effectName + "' not found!");
		}
		soundEffectSource.PlayOneShot(clip);
	}

	public void Pause()
	{
		bgmSource.Pause();
	}

	public void UnPause()
	{
		bgmSource.UnPause();
	}

	public void SetTimescale(float scale){
		bgmSource.pitch = scale;
		soundEffectSource.pitch = scale;
	}
}
}