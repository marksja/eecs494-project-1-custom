﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class Barrier : MonoBehaviour
{
	void OnEnable()
	{
		GameController.audioManager.Play("Door_Unlock");
	}

	void OnDisable()
	{
		GameController.audioManager.Play("Door_Unlock");
	}
}
}