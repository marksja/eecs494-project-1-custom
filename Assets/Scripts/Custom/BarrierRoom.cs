﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class BarrierRoom : MonoBehaviour
{
	public GameObject barrier;

	RoomManager roomManager;

	void Start()
	{
		roomManager = GetComponent<RoomManager>();
		StartCoroutine(RoomCheck());
	}
	
	IEnumerator RoomCheck()
	{
		while (!roomManager.isCleared()) {
			yield return new WaitForSeconds(0.1f);
		}
		barrier.SetActive(false);
	}
}
}