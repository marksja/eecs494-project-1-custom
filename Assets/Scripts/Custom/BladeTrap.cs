﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class BladeTrap : MonoBehaviour {
	public float speed;
	public float return_speed;

	Vector3 starting_pos;

	bool hit_object;

	// Use this for initialization
	void Start () {
		starting_pos = transform.position;

		hit_object = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position == starting_pos){
			RaycastHit[] hits = Physics.RaycastAll(transform.position, Vector3.up, 16);
			foreach(RaycastHit hit in hits){
				if(hit.collider.gameObject.tag == "Player"){
					StartCoroutine(MoveInDirection(Vector3.up));
				}
			}
			hits = Physics.RaycastAll(transform.position, Vector3.down, 16);
			foreach(RaycastHit hit in hits){
				if(hit.collider.gameObject.tag == "Player"){
					StartCoroutine(MoveInDirection(Vector3.down));
				}
			}
			hits = Physics.RaycastAll(transform.position, Vector3.left, 16);
			foreach(RaycastHit hit in hits){
				if(hit.collider.gameObject.tag == "Player"){
					StartCoroutine(MoveInDirection(Vector3.left));
				}
			}
			hits = Physics.RaycastAll(transform.position, Vector3.right, 16);
			foreach(RaycastHit hit in hits){
				if(hit.collider.gameObject.tag == "Player"){
					StartCoroutine(MoveInDirection(Vector3.right));
				}
			}
		}
	}

	IEnumerator MoveInDirection(Vector3 direction){
		while(!hit_object){
			transform.Translate(direction * speed * Time.deltaTime);
			yield return new WaitForEndOfFrame();
		}

		hit_object = false;
		StartCoroutine(ReturnToStartingPosition());
	}

	IEnumerator ReturnToStartingPosition(){
		Vector3 unit_direction;
		
		while(Vector3.Distance(transform.position, starting_pos) > 0.01){
			unit_direction = starting_pos - transform.position;
			unit_direction = Vector3.Normalize(unit_direction);
			transform.Translate(unit_direction * return_speed * Time.deltaTime);
			yield return new WaitForEndOfFrame();
		}

		transform.position = starting_pos;
	}

	void OnTriggerEnter(Collider hit)
	{	
		if(!hit_object && (hit.gameObject.tag == "Wall" || hit.gameObject.tag == "Enemy")){
			hit_object = true;
		}
	}
}
}