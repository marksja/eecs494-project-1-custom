﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class BombExplosion : MonoBehaviour
{
	public int damage;
	public float activeTime;

	void Start()
	{
		StartCoroutine(Init());
	}
	
	// Have to spawn off screen then move to position in order to fire OnTriggerEnter
	IEnumerator Init()
	{
		Vector3 target = transform.position;
		transform.position = new Vector3(-100, -100, 0);
		yield return new WaitForEndOfFrame();
		transform.position = target;
		GameController.audioManager.Play("Bomb_Blow");
		yield return new WaitForSeconds(activeTime);
		GetComponent<BoxCollider>().enabled = false;
		yield return new WaitForSeconds(0.15f);
		Destroy(this.gameObject);
	}

	void OnTriggerEnter(Collider other)
	{
		Health health = other.GetComponent<Health>();
		if (health != null && other.tag != "Player") {
			health.DealDamage(damage, transform.position - health.gameObject.transform.position);
		}
	}
}
}