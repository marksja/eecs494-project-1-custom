﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class CameraBounds : MonoBehaviour
{
	public Vector2 startRoom;
	public float speed = 5;
	public GameObject currentRoomItems;
	public PlayerScript player;

	Vector2 leftVector;
	Vector2 rightVector;
	Vector2 upVector;
	Vector2 downVector;

	Vector3 leftBound;
	Vector3 rightBound;
	Vector3 upBound;
	Vector3 downBound;

	bool canMove = true;

	public bool disabled;
	public Vector2 currentRoom;

	public static CameraBounds Instance;

	void Start()
	{
		currentRoom = startRoom;

		leftVector = new Vector2(-16, 0);
		rightVector = new Vector2(16, 0);
		downVector = new Vector2(0, -11);
		upVector = new Vector2(0, 11);

		leftBound = new Vector3(-6.5f, 0, 0);
		rightBound = new Vector3(6.5f, 0, 0);
		downBound = new Vector3(0, -5f, 0);
		upBound = new Vector3(0, 1f, 0);

		Instance = this;
	}

	public void MoveCamera(string direction)
	{
		if (canMove && !disabled) {
			canMove = false;
			Vector2 moveDir = DirectionFromString(direction);
			StartCoroutine(MoveCameraHelper(moveDir));
		}
	}

	Vector2 DirectionFromString(string direction)
	{
		if (direction == "Up") return upVector;
		if (direction == "Down") return downVector;
		if (direction == "Left") return leftVector;
		if (direction == "Right") return rightVector;
		return Vector2.zero;
	}

	IEnumerator MoveCameraHelper(Vector2 direction)
	{
		player.DisableMovement();
        GameController.instance.OnRoomExit(currentRoom);
        // Set current room
		Vector2 nextRoom = currentRoom;
        if (direction.y > 0) {
            ++nextRoom.y;
        }
        else if (direction.y < 0) {
            --nextRoom.y;
        }
        if (direction.x > 0) {
            ++nextRoom.x;
        }
        else if (direction.x < 0) {
            --nextRoom.x;
        }
		GameController.instance.BeforeRoomEnter(nextRoom);
		// Short delay to match the original
		yield return new WaitForSeconds(0.004f);
		Vector3 target = transform.position + (Vector3)direction;
		float distanceToTravel = direction.magnitude;
		while (distanceToTravel > 0) {
			Vector3 moveVec = direction.normalized * speed * Time.deltaTime;
			transform.Translate(moveVec);
			distanceToTravel -= moveVec.magnitude;
			yield return new WaitForEndOfFrame();
		}
		transform.position = target;
		// move factor is special for moving up, since we touch the bounds with the player's legs only
		//   taking an extra 0.5 units of space compared to the other directions
		float moveFac = 2;
		if (player.facing_direction == PlayerScript.Direction.Up) {
			moveFac = 1.5f;
		}
		yield return player.MoveToPoint(
			player.transform.position +
			(Vector3)(moveFac * player.VectorFromDirection(player.facing_direction))
		);
		canMove = true;
		currentRoom = nextRoom;
		player.EnableMovement();
		GameController.instance.AfterRoomEnter(currentRoom);
	}

	public bool isWithinBounds(Vector3 pos){
	// 	if(transform.position.x < Camera.main.transform.position.x - 5.5){
	// 	}
	// 	if(transform.position.x > Camera.main.transform.position.x + 5.5){
	// 	}
	// 	if(transform.position.y < Camera.main.transform.position.y - 5.5){
	// 	} 
	// 	if(transform.position.y > Camera.main.transform.position.y + 1.5){
	// 	}

		if(pos.x > (Camera.main.transform.position + (Vector3)rightBound).x){
			return false;
		}
		if(pos.x < (Camera.main.transform.position + (Vector3)leftBound).x){
			return false;
		}
		if(pos.y > (Camera.main.transform.position + (Vector3)upBound).y){
			return false;
		}
		if(pos.y < (Camera.main.transform.position + (Vector3)downBound).y){
			return false;
		}
		return true;
	}
}
}