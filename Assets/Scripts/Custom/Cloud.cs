﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class Cloud : MonoBehaviour
{
	Animator anim;

	void Start()
	{
		anim = GetComponent<Animator>();
		StartCoroutine(DestroyAfterTime());
	}
	
	IEnumerator DestroyAfterTime()
	{
		float aliveTime = anim.GetCurrentAnimatorStateInfo(0).length;
		yield return new WaitForSeconds(aliveTime);
		Destroy(this.gameObject);
	}
}
}