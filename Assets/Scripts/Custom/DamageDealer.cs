﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class DamageDealer : MonoBehaviour {

	public int damage_amount;

	public bool apply_knockback = true;

	public string[] ignored_tags;

	// Use this for initialization
	void Start () {

	}

	void OnCollisionEnter(Collision collisionInfo){
		foreach(string tag in ignored_tags){
			if(collisionInfo.gameObject.tag == tag) {
				return;
			}
		}
		if(collisionInfo.gameObject.GetComponent<Health>() != null){
			Vector3 push_direction = transform.position - collisionInfo.gameObject.transform.position;
			collisionInfo.gameObject.GetComponent<Health>().DealDamage(damage_amount, push_direction, apply_knockback);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		foreach(string tag in ignored_tags){
			if(other.gameObject.tag == tag) {
				return;
			}
		}
		if(other.gameObject.GetComponent<Health>() != null){
			Vector3 push_direction = transform.position - other.gameObject.transform.position;
			other.gameObject.GetComponent<Health>().DealDamage(damage_amount, push_direction, apply_knockback);
		}
	}


}
}