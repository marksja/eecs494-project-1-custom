﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class JumpToRoom : MonoBehaviour
{
	public Vector2 room;
	[Tooltip("Enable in-game to teleport to the given room")]
	public bool GO = false;
	[Space(10)]
	public GameObject player;
    public Vector3 playerOffset;
    public Vector3 camOffset;

	void Update()
	{
		if (GO) {
			GO = false;
			Transform level = GameObject.FindGameObjectWithTag("Level").transform;
			Transform target = level.Find("Room (" + room.x + "," + room.y + ")");
			if (target != null) {
				Debug.Log("Moving camera");
				CameraBounds cameraBounds = GetComponent<CameraBounds>();
				GameController.instance.OnRoomExit(cameraBounds.currentRoom);
				GameController.instance.BeforeRoomEnter(room);
				Vector3 targetPos = target.transform.position;
				Camera.main.transform.position = targetPos + camOffset;
				player.transform.position = targetPos + playerOffset;
				cameraBounds.currentRoom = room;
				GameController.instance.AfterRoomEnter(room);
			}
		}
	}
}
}