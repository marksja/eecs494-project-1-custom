﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class Fireball : MonoBehaviour {

	Vector3 target;
	Vector3 starting_point;
	
	float speed = 0.1f;
	float time;

	void Start()
	{
		time = 0;
		starting_point = transform.position;
	}
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.Lerp(starting_point, target, time * speed);
		time += Time.deltaTime;

		if(!CameraBounds.Instance.isWithinBounds(transform.position)){
			Destroy(gameObject);
		}
	}

	public void SetTarget(Vector3 destination){
		target = destination;
	}
}
}