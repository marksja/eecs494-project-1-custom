﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class ForceNESResolution : MonoBehaviour
{
	const int NESwidth = 256;
	const int NESheight = 240;

	public int scale;

	void Awake()
	{
		Screen.SetResolution(scale * NESwidth, scale * NESheight, false);
	}
}
}