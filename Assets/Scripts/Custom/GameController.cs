﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ZeldaCustom {
public class GameController : MonoBehaviour
{
	public static GameController instance;
	public static AudioManager audioManager;

	public Image weaponB;
	public PlayerScript player;
	public GameObject smokePuffPrefab;
	public Image blackImg;
	public bool hasClock = false;

	Inventory player_inventory;
	Health player_health;

	public bool cheat_mode;
	int pre_cheat_rupees;
	int pre_cheat_bombs;
	int pre_cheat_keys;

	bool gameStart = true;

	Dictionary<Vector2, List<IRoomEventHandler>> roomSpawnObjects;

	// Use this for initialization
	void Awake () {
		if (instance != this) {
            if (instance == null) {
				instance = this;
			}
            else {
				Destroy(this.gameObject);
			}
        }
		roomSpawnObjects = new Dictionary<Vector2, List<IRoomEventHandler>>();
	}

	void Start () {
		audioManager = GetComponent<AudioManager>();
		player_inventory = player.GetComponent<Inventory>();
		player_health = player.GetComponent<Health>();
	}
	
	// Update is called once per frame
	void Update () {
		// Hack for initial room, since we don't "Enter" it
		if (gameStart) {
			gameStart = false;
			Vector2 startingRoom = new Vector2(1, 0);
			BeforeRoomEnter(startingRoom);
			AfterRoomEnter(startingRoom);
		}
		if(cheat_mode){
			if(player_inventory.Rupees < 999){
				player_inventory.AddRupees(999 - player_inventory.Rupees);
			}
			if(player_inventory.Bombs < 99){
				player_inventory.AddBombs(99 - player_inventory.Bombs);
			}
			if(player_inventory.Keys < 99){
				player_inventory.AddKeys(99 - player_inventory.Keys);
			}
		}
		if(Input.GetKeyDown(KeyCode.Alpha1)){
			if(cheat_mode){
				player_inventory.AddRupees(pre_cheat_rupees - 999);
				player_inventory.AddBombs(pre_cheat_bombs - 99);
				player_inventory.AddKeys(pre_cheat_keys - 99);
				player_health.isDamageable = true;
				cheat_mode = false;
			}
			else if(!cheat_mode){
				pre_cheat_rupees = player_inventory.Rupees;
				pre_cheat_bombs = player_inventory.Bombs;
				pre_cheat_keys = player_inventory.Keys;
				player_health.isDamageable = false;
				player_health.AddHealth(100);
				cheat_mode = true;
			}
		}
		if (Input.GetKeyDown(KeyCode.Alpha2)) {
			player.AddWeapon(PlayerScript.Weapon.Bow);
		}
		if (Input.GetKeyDown(KeyCode.Alpha3)) {
			player.AddWeapon(PlayerScript.Weapon.Boomerang);
		}
	}

	public void RegisterHandler(Vector2 room, IRoomEventHandler eventHandler)
	{
		List<IRoomEventHandler> eventHandlers;
        if (!roomSpawnObjects.TryGetValue(room, out eventHandlers)) {
            eventHandlers = roomSpawnObjects[room] = new List<IRoomEventHandler>();
        }
		eventHandlers.Add(eventHandler);
	}

	public void DeregisterHandler(Vector2 room, IRoomEventHandler eventHandler)
	{
        List<IRoomEventHandler> eventHandlers;
        if (!roomSpawnObjects.TryGetValue(room, out eventHandlers)) {
            eventHandlers = roomSpawnObjects[room] = new List<IRoomEventHandler>();
        }
        eventHandlers.Remove(eventHandler);
    }

	public void BeforeRoomEnter(Vector2 room)
	{
		List<IRoomEventHandler> eventHandlers;
        if (!roomSpawnObjects.TryGetValue(room, out eventHandlers)) {
            return;
        }
		foreach (IRoomEventHandler eventHandler in eventHandlers) {
			eventHandler.BeforeRoomEnter();
		}
	}

	public void AfterRoomEnter(Vector2 room)
    {
		List<IRoomEventHandler> eventHandlers;
        if (!roomSpawnObjects.TryGetValue(room, out eventHandlers)) {
            return;
        }
        foreach (IRoomEventHandler eventHandler in eventHandlers) {
            eventHandler.AfterRoomEnter();
        }
    }

	public void OnRoomExit(Vector2 room)
	{
		List<IRoomEventHandler> eventHandlers;
        if (!roomSpawnObjects.TryGetValue(room, out eventHandlers)) {
            return;
        }
		foreach (IRoomEventHandler eventHandler in eventHandlers) {
            eventHandler.OnRoomExit();
        }
	}

	public void OnPlayerDeath()
	{
		Vector2 room = Camera.main.GetComponent<CameraBounds>().currentRoom;
		List<IRoomEventHandler> eventHandlers;
        if (!roomSpawnObjects.TryGetValue(room, out eventHandlers)) {
            return;
        }
        foreach (IRoomEventHandler eventHandler in eventHandlers) {
            eventHandler.OnPlayerDeath();
        }
	}

	[ContextMenu("Reset Level")]
	public void ResetLevel(){
		RoomManager[] rooms = GameObject.FindGameObjectWithTag("Level").GetComponentsInChildren<RoomManager>();
		foreach(RoomManager room in rooms){
			if(!room.isCleared()){
				room.ResetRoom();
			}
		}
		//Camera.main.GetComponent<JumpToRoom>().room = CameraBounds.Instance.currentRoom;
		//Camera.main.GetComponent<JumpToRoom>().GO = true;
		player.gameObject.transform.position = new Vector3((CameraBounds.Instance.currentRoom.x * 16) + 1, 5, 0);
		player.GetComponent<PlayerScript>().Live();
	}

	public IEnumerator FadeToBlack(float duration)
	{
		float t = 0;
		Color c;
		while (t < duration) {
			t += Time.deltaTime;
			c = blackImg.color;
			c.a = Mathf.Lerp(0, 1, t / duration);
			blackImg.color = c;
			yield return new WaitForEndOfFrame();
		}
		c = blackImg.color;
		c.a = 1f;
		blackImg.color = c;
	}

	public IEnumerator FadeFromBlack(float duration)
	{
		float t = 0;
        Color c;
        while (t < duration)
        {
            t += Time.deltaTime;
            c = blackImg.color;
            c.a = Mathf.Lerp(1, 0, t / duration);
            blackImg.color = c;
            yield return new WaitForEndOfFrame();
        }
        c = blackImg.color;
        c.a = 0f;
        blackImg.color = c;
	}

	public void GetClock(Sprite clockSprite)
	{
		hasClock = true;
		weaponB.sprite = clockSprite;
	}
}
}