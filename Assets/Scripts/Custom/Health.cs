﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ZeldaCustom {
public class Health : MonoBehaviour {

	public UnityEvent call_when_hit;
	public UnityEvent hit_finished;

	public int health;
	public int current_health;

	public float iframes_time;
	public float knockback_distance;
	public float knockback_time;

	public Vector3 knockback_offset;

	public bool isDamageable;

	SpriteRenderer sr;
	Color base_color;
	public Color desired_color;
	float time = 0.0f;

	public bool isPlayer = false;

	// Use this for initialization
	void Start () {
		current_health = health;
		isDamageable = true;
		sr = GetComponent<SpriteRenderer>();
		base_color = sr.color;
	}
	
	void Update(){
		if(isDamageable == false){
			SpriteRenderer sr = GetComponent<SpriteRenderer>();
			sr.color = Color.Lerp(base_color, desired_color, Mathf.PingPong(time * 20, 1));
			time += Time.deltaTime;
		}
		else{
			if(sr.color != base_color){
				sr.color = base_color;
				time = 0.0f;
			}
		}
	}

	// Update is called once per frame
	public void AddHealth(int amount){
		current_health += amount;
		if(current_health > health) current_health = health;
	}

	public int GetCurrentHP(){
		return current_health;
	}

	public void DealDamage(int damage, Vector3 direction, bool doKnockback = true){
		if(!isDamageable || current_health == 0) return;
		current_health -= damage;
		if(current_health <= 0){
			current_health = 0;
			if (!isPlayer) {
				if (GetComponent<Aquamentus>() != null) {
					GameController.audioManager.Play("Boss_Scream3");
				}
				else {
					GameController.audioManager.Play("Enemy_Die");
				}
			}
		}
		else{
			isDamageable = false;
			call_when_hit.Invoke();
			if(doKnockback){
				StartCoroutine(Knockback(direction));
			}	
			StartCoroutine(IFrames());
			if (isPlayer) {
				GameController.audioManager.Play("Link_Hurt");
			}
			else if (GetComponent<Aquamentus>() != null) {
				GameController.audioManager.Play("Boss_Hit");
			}
			else {
				GameController.audioManager.Play("Enemy_Hit");
			}
		}
	}

	IEnumerator Knockback(Vector3 direction){
		direction.x = (Mathf.Abs(direction.y) > Mathf.Abs(direction.x)) ? 0 : -1 * Mathf.Sign(direction.x);
		direction.y = (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)) ? 0 : -1 * Mathf.Sign(direction.y);
		Vector3 next_location;
	
		float distance = knockback_distance;
		
		//Raycast at 3 different points, the center and the extremes of the directions we're traveling in
		RaycastHit[] hits = Physics.RaycastAll(transform.position, direction, distance);

		for(int i = 0; i < hits.Length; ++i){
			if(hits[i].collider.gameObject.tag == "Wall"){
				distance = 0;
				break;
			}
		}

		hits = Physics.RaycastAll(transform.position + ((direction.x == 0) ? Vector3.left * .49f : Vector3.up *.5f), direction, distance);

		for(int i = 0; i < hits.Length; ++i){
			if(hits[i].collider.gameObject.tag == "Wall"){
				distance = 0;
				break;
			}
		}

		hits = Physics.RaycastAll(transform.position + ((direction.x == 0) ? Vector3.right *.49f : Vector3.down * .5f), direction, distance);

		for(int i = 0; i < hits.Length; ++i){
			if(hits[i].collider.gameObject.tag == "Wall"){
				distance = 0;
				break;
			}
		}
		next_location = distance * direction;
		knockback_offset = next_location;
		next_location += transform.position;
		Vector3 last_pos = transform.position;
		float time = 0.0f;
		while(time < knockback_time){
			transform.position = Vector3.Lerp(last_pos, next_location, time / knockback_time);
			time += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		knockback_offset = Vector3.zero;
		hit_finished.Invoke();
	}

	IEnumerator IFrames(){
		yield return new WaitForSeconds(iframes_time);
		isDamageable = true;
		
	}

	public void Stun(float duration)
	{
		StartCoroutine(StunHelper(duration));
	}

	IEnumerator StunHelper(float duration)
	{
		call_when_hit.Invoke();
		yield return new WaitForSeconds(duration);
		hit_finished.Invoke();
	}
}
}