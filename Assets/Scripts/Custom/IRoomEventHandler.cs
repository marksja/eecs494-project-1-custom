namespace ZeldaCustom {
// These events are called by GameContoller when we enter and exit rooms. Be sure to register
// your concrete class with the GameController on Start() so it knows to call these!
public interface IRoomEventHandler
{
    void BeforeRoomEnter();
    void AfterRoomEnter();
    void OnRoomExit();
    void OnPlayerDeath();
}
}