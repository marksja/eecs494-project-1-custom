﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ZeldaCustom {
public class Inventory : MonoBehaviour
{
	public Text rupeeText;
	public Text keyText;
	public Text bombText;

	public int Rupees { get; private set; }
	public int Bombs { get; private set; }
	public int Keys { get; private set; }

	public void AddRupees(int amt)
	{
		Rupees += amt;
		rupeeText.text = "X" + Rupees;
	}

	public void AddBombs(int amt)
	{
		Bombs += amt;
		bombText.text = "X" + Bombs;
	}

	public void AddKeys(int amt)
	{
		Keys += amt;
		keyText.text = "X" + Keys;
	}
}
}