﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class ItemCollector : MonoBehaviour
{
	Inventory inventory;
	Health health;
	PlayerScript player;

	void Start()
	{
		player = GameObject.Find("Player").GetComponent<PlayerScript>();
		inventory = player.GetComponent<Inventory>();
		health = player.GetComponent<Health>();
	}

	void OnTriggerEnter(Collider other)
	{
		
		if (other.tag == "Rupee") {
			GameController.audioManager.Play("Get_Rupee");
			inventory.AddRupees(1);
		}
		else if (other.tag == "Bomb") {
			GameController.audioManager.Play("Get_Heart");
			inventory.AddBombs(4);
		}
		else if (other.tag == "Key") {
			GameController.audioManager.Play("Get_Heart");
			inventory.AddKeys(1);
		}
		else if (other.tag == "Heart") {
			GameController.audioManager.Play("Get_Heart");
			health.AddHealth(2);
		}
		else if (other.tag == "ItemBow") {
			GameController.audioManager.Play("Fanfare");
			player.AddWeapon(PlayerScript.Weapon.Bow);
		}
		else if (other.tag == "ItemBoomerang") {
			GameController.audioManager.Play("Fanfare");
			player.AddWeapon(PlayerScript.Weapon.Boomerang);
		}
		else if (other.tag == "ItemClock") {
			GameController.audioManager.Play("Fanfare");
			GameController.instance.GetClock(other.GetComponent<SpriteRenderer>().sprite);
		}
		else {
			return;
		}
		Destroy(other.gameObject);
	}
}
}