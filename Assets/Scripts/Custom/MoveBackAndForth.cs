﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class MoveBackAndForth : MonoBehaviour
{
	public Vector2 startDir;
	public float distance;
	public float speed;

	Vector2 direction;

	void Start()
	{
		direction = startDir;
		StartCoroutine(HealthCheck());
		StartCoroutine(Move());
	}

	IEnumerator HealthCheck()
	{
		Health health = GetComponent<Health>();
		if (health == null) { yield break; }
		while (health.current_health > 0) {
			yield return new WaitForEndOfFrame();
		}
		Destroy(this.gameObject);
	}
	
	IEnumerator Move()
	{
		float distanceTravelled = 0;
		while (distanceTravelled < distance) {
			Vector3 toMove = direction * speed * Time.deltaTime;
			transform.Translate(toMove);
			distanceTravelled += toMove.magnitude;
			yield return new WaitForEndOfFrame();
		}
		direction *= -1;
		StartCoroutine(Move());
	}
}
}