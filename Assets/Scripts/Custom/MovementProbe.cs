﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ZeldaCustom {
public class MovementProbe : MonoBehaviour
{
	public bool Touching { get { return touching > 0; } }
	[HideInInspector]
	public UnityEvent onWallTouch;

	int touching = 0;
	ArrowKeyMovement movement;
	PlayerScript player;

	void Start()
	{
		movement = GetComponentInParent<ArrowKeyMovement>();
		player = GetComponentInParent<PlayerScript>();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Wall") {
			++touching;
            onWallTouch.Invoke();
        }
		else if (other.tag == "CameraBounds") {
			Camera.main.GetComponent<CameraBounds>().MoveCamera(other.name);
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Wall") {
            --touching;
			if (touching < 0) {
				touching = 0;
			}
        }
	}

	// void OnCollisionEnter(Collision other)
	// {
	// 	if (other.collider.tag != "PlayerProbe") {
	// 		touching = true;
	// 	}
	// }

	// void OnCollisionExit(Collision other)
	// {
	// 	if (other.collider.tag != "PlayerProbe") {
	// 		touching = false;
	// 	}
	// }
}
}