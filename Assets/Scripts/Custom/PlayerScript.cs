﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ZeldaCustom {
public class PlayerScript : MonoBehaviour {

	public enum Direction { Left, Right, Up, Down };
	public enum Weapon { Bow, Bomb, Boomerang };

	[System.Serializable]
	public class AttackingSprites
	{
		public Sprite left;
		public Sprite up;
		public Sprite right;
		public Sprite down;
	}

	[System.Serializable]
	public class SwordOffsets
	{
		public Vector3 left;
		public Vector3 up;
		public Vector3 right;
		public Vector3 down;
	}

	public Direction facing_direction;

	Health player_health;
	ArrowKeyMovement movement;
	public Inventory inventory;

	public Weapon currentWeapon;
	public List<Weapon> availableWeapons;

	public GameObject sword;
	public GameObject bomb;
	public GameObject boomerang;
	public GameObject arrow;

	public WeaponSelection weaponSelection;

	public SwordOffsets swordOffsets;
	public AttackingSprites attackingSprites;

	GameObject sword_instance;
	GameObject boomerang_instance;
	GameObject arrow_instance;
	GameObject bomb_instance;
	bool attack_disabled = false;
	Sprite lastFacingSprite;
	bool dying = false;
	
	// Use this for initialization
	void Start () {
		facing_direction = Direction.Up; 
		player_health = GetComponent<Health>();
		movement = GetComponent<ArrowKeyMovement>();
		inventory = GetComponent<Inventory>();
	}
	
	// Update is called once per frame
	void Update () {
		if (dying) return;

		if (player_health.current_health == 0) {
			dying = true;
			StartCoroutine(Die());
			return;
		}

		facing_direction = movement.last_direction;

		if (!attack_disabled && Input.GetButtonDown("A")) {
			if (sword_instance == null) {
				StartCoroutine(Attack(sword));
			}
		}
	}

	// Plays attack animation and spawns the given weapon in front of link
	IEnumerator Attack(GameObject weaponPrefab)
	{
		//DisableMovement();
		Vector3 spawn_pos = transform.position;
        Quaternion direction = Quaternion.identity;
		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		lastFacingSprite = sr.sprite;
        switch (facing_direction)
        {
            case Direction.Up:
				if (weaponPrefab == sword) {
                	spawn_pos += swordOffsets.up;
				}
				else {
					spawn_pos += Vector3.up;
				}
                direction = Quaternion.AngleAxis(0, Vector3.forward);
				sr.sprite = attackingSprites.up;
                break;
            case Direction.Down:
                if (weaponPrefab == sword) {
                    spawn_pos += swordOffsets.down;
                }
                else {
                    spawn_pos += Vector3.down;
                }
                direction = Quaternion.AngleAxis(180, Vector3.forward);
				sr.sprite = attackingSprites.down;
                break;
            case Direction.Right:
                if (weaponPrefab == sword) {
                    spawn_pos += swordOffsets.right;
                }
                else {
                    spawn_pos += Vector3.right;
                }
                direction = Quaternion.AngleAxis(270, Vector3.forward);
				sr.sprite = attackingSprites.right;
                break;
            case Direction.Left:
                if (weaponPrefab == sword) {
                    spawn_pos += swordOffsets.left;
                }
                else {
                    spawn_pos += Vector3.left;
                }
                direction = Quaternion.AngleAxis(90, Vector3.forward);
				sr.sprite = attackingSprites.left;
                break;
            default:
                Debug.Log("No direction. Everything is ruined");
                break;
        }
		yield return new WaitForEndOfFrame();
		DisableMovement();
		GameObject spawned = Instantiate(weaponPrefab, spawn_pos, direction);
		if (weaponPrefab == sword) {
			sword_instance = spawned;
			sword_instance.transform.parent = transform;
			sword_instance.GetComponent<Sword>().on_destroy.AddListener(AfterAttack);
		}
	}

	void AfterAttack()
	{
		StartCoroutine(AfterAttackHelper());
	}

	IEnumerator AfterAttackHelper()
	{
		yield return new WaitForEndOfFrame();
		EnableMovement();
        GetComponent<SpriteRenderer>().sprite = lastFacingSprite;
	}

	IEnumerator Die()
	{
		DisableMovement();
		transform.SetParent(null);	// Detach from wallmaster
		CameraBounds.Instance.disabled = false;
		GameController.instance.OnPlayerDeath();
		yield return new WaitForEndOfFrame();
		Animator anim = GetComponent<Animator>();
		anim.enabled = true;
		anim.Play("LinkDie");
		yield return new WaitForSeconds(1.0f);
		GameController.audioManager.Play("Link_Die");
		yield return new WaitForSeconds(1.0f);
		StartCoroutine(GameController.instance.FadeToBlack(2.0f));
		yield return new WaitForSeconds(2.0f);
		anim.enabled = false;
		yield return new WaitForSeconds(1.0f);
		player_health.current_health = player_health.health;
		GameController.instance.ResetLevel();
	}

	public void Live(){
		Animator anim = GetComponent<Animator>();
		dying = false;
		StartCoroutine(GameController.instance.FadeFromBlack(2.0f));
		StartCoroutine(EnableMovementAfterSeconds(2.0f));
	}

	IEnumerator EnableMovementAfterSeconds(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		EnableMovement();
		GameController.instance.AfterRoomEnter(CameraBounds.Instance.currentRoom);
	}

	public IEnumerator MoveToPoint(Vector2 target)
	{
		DisableMovement();
		Vector2 direction = target - (Vector2)transform.position;
		Vector2 moved = Vector2.zero;
		// First move vertically towards the target
		while (Mathf.Abs(moved.y) < Mathf.Abs(direction.y)) {
			Vector2 trueDir = direction.y > 0 ? Vector2.up : Vector2.down;
			moved += movement.Move(trueDir);
			yield return new WaitForEndOfFrame();
		}
		movement.SnapVertical();
		// Then move horizontally towards the target
		while (Mathf.Abs(moved.x) < Mathf.Abs(direction.x)) {
			Vector2 trueDir = direction.x > 0 ? Vector2.right : Vector2.left;
			moved += movement.Move(trueDir);
			yield return new WaitForEndOfFrame();
		}
		movement.SnapHorizontal();
		EnableMovement();
	}

    public Vector2 VectorFromDirection(Direction direction)
    {
        switch (direction)
        {
            case Direction.Down: return Vector2.down;
            case Direction.Left: return Vector2.left;
            case Direction.Right: return Vector2.right;
            case Direction.Up: return Vector2.up;
        }
        throw new UnityException("Error in PlayerScript.VectorFromDirection");
    }

	public void DisableMovement(){
		movement.DisableControls();
		attack_disabled = true;
	}

	public void EnableMovement(){
		movement.EnableControls();
		attack_disabled = false;
	}

	public void DisableAttack(){
		attack_disabled = true;
	}

	public void EnableAttack(){
		attack_disabled = false;
	}

	public bool CanFireMagicSword() {
		return player_health.current_health == player_health.health;
	}

	public void AddWeapon(Weapon weapon)
	{
		if (availableWeapons.Contains(weapon)) {
			return;
		}
		availableWeapons.Add(weapon);
		weaponSelection.InitSelectionDisplay();
	}
}
}