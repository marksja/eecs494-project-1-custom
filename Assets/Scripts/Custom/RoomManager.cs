﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class RoomManager : MonoBehaviour {

	//static RoomManager currentroom;
	Spawner[] spawners;

	// Use this for initialization
	void Start () {
		spawners = GetComponentsInChildren<Spawner>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	[ContextMenu ("Is it Cleared?")]
	void PrintState(){
		Debug.Log(isCleared());
	}

	public bool isCleared(){
		foreach (Spawner spawner in GetComponentsInChildren<Spawner>()) {
			if (spawner.prefabToSpawn.name != "BladeTrap" && spawner.prefabToSpawn.name != "Fire") {
				return false;
			}
		}
		return true;
	}

	public void ResetRoom(){
		foreach(Spawner spawner in spawners){
			spawner.gameObject.SetActive(true);
		}
	}
}
}