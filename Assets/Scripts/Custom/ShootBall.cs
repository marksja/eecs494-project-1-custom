﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class ShootBall : MonoBehaviour {

	GameObject player;
	public GameObject ball;
	public float min_delay;
	public float max_delay;
	float time;
	float current_delay;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		
		current_delay = Random.Range(min_delay, max_delay);
		time = current_delay;
	}

	void Update(){
		time += Time.deltaTime;
		if(time > current_delay){
			Vector3 base_target = Vector3.Normalize(player.transform.position - transform.position) * 30;
			time = 0;
			current_delay = Random.Range(min_delay, max_delay);
			GameController.audioManager.Play("Candle");
			GameObject fire_temp = Instantiate(ball, transform.position, Quaternion.identity);
			fire_temp.transform.parent = transform;
			fire_temp.GetComponent<Fireball>().SetTarget(transform.position + base_target);
		}
	}
}
}