﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class Spawner : MonoBehaviour, IRoomEventHandler
{
	public Vector2 room;
	public GameObject prefabToSpawn;

	bool active;
	GameObject spawned;

	void Start()
	{
		GameController.instance.RegisterHandler(room, this);
	}

	void Update()
	{
		if (active && spawned == null) {
			// Our prefab was destroyed before we despawned it. It must have been destroyed by the player
			// Do not respawn it, instead disable ourself
			gameObject.SetActive(false);
			active = false;
		}
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(transform.position, 0.2f);
	}
	
	public void Spawn()
	{
		StartCoroutine(SpawnHelper());
	}

	IEnumerator SpawnHelper()
	{
		// Spawn cloud
		GameObject smoke = Instantiate(GameController.instance.smokePuffPrefab, transform.position, Quaternion.identity);
		yield return new WaitForSeconds(0.25f);
		Destroy(smoke);
		spawned = Instantiate(prefabToSpawn, transform.position, Quaternion.identity);
		active = true;
	}

	public void Despawn()
	{
		active = false;
		Destroy(spawned);
	}

	void IRoomEventHandler.BeforeRoomEnter()
	{ 
		// Do nothing
	}

	void IRoomEventHandler.AfterRoomEnter()
	{
		Spawn();
	}

	void IRoomEventHandler.OnRoomExit()
	{
		Despawn();
	}

    public void OnPlayerDeath()
    {
        Despawn();
    }
}
}