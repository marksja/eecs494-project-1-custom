﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class Stalfos : MonoBehaviour {

	Health hp;
	SpriteRenderer sr;

	GameObject player;

	Vector3 last_position;
	Vector3 next_destination;

	public List<int> direction_queue;
	int last_direction = 0;
	float time;
	bool move_failed;
	bool moved_up_last;
	bool move_disabled;
	int cached_health;

	public float speed;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
		hp = GetComponent<Health>();
		sr = GetComponent<SpriteRenderer>();
		next_destination = transform.position;
		direction_queue = new List<int>();
	}
	
	// Update is called once per frame
	void Update () {
		if(hp.current_health == 0){
			Destroy(gameObject);
		}

		if(move_disabled || !sr.isVisible){
			return;
		}

		if(Vector3.Distance(transform.position, next_destination) != 0){
			Vector3 direciton_to_target = next_destination - transform.position;
			float angle = Vector3.SignedAngle(Vector3.up, direciton_to_target, Vector3.back);
			angle = Mathf.Round(angle / 90);
			if(angle == -1) angle = 3;
			if(angle == -2) angle = 2;
			GetComponent<Animator>().SetInteger("direction", (int)angle + 1);
			transform.position = Vector3.Lerp(last_position, next_destination, time * speed);
		}
		else{
			time = 0;
			Vector3 path;
			int direction = 0;
			if(direction_queue.Count == 0){
				if(moved_up_last){
					if((player.transform.position - this.transform.position).x < -0.5f){
						direction = 1;
						moved_up_last = false;
					}
					else if((player.transform.position - this.transform.position).x > 0.5f){
						direction = 3;
						moved_up_last = false;
					}
					else if((player.transform.position - this.transform.position).y < -.5f){
						direction = 2;
						moved_up_last = true;
					}
					else if((player.transform.position - this.transform.position).y > 0.5f){
						direction = 0;
						moved_up_last = true;
					}
				}
				else{
					if((player.transform.position - this.transform.position).y < -0.5f){
						direction = 2;
						moved_up_last = true;
					}
					else if((player.transform.position - this.transform.position).y > 0.5f){
						direction = 0;
						moved_up_last = true;
					}
					else if((player.transform.position - this.transform.position).x < -0.5f){
						direction = 1;
						moved_up_last = false;
					}
					else if((player.transform.position - this.transform.position).x > 0.5f){
						direction = 3;
						moved_up_last = false;
					}
				}
				
			}
			else{
				direction = direction_queue[0];
				direction_queue.RemoveAt(0);
			}
			last_direction = direction;
			switch(direction){
				case 0:
					//GetComponent<Animator>().SetInteger("direction", 1);
					path = Vector3.up;
					break;
				case 1:
					//GetComponent<Animator>().SetInteger("direction", 4);
					path = Vector3.left;
					break;
				case 2:
					//GetComponent<Animator>().SetInteger("direction", 3);
					path = Vector3.down;
					break;
				case 3:
					//GetComponent<Animator>().SetInteger("direction", 2);
					path = Vector3.right;
					break;
				default:
					Debug.Log("The end has come!");
					path = Vector3.zero;
					break;
			}
			next_destination = transform.position + path;
			last_position = this.transform.position;
			move_failed = false;
			time += Time.deltaTime;
			transform.position = Vector3.Lerp(last_position, next_destination, time * speed);
		}
		time += Time.deltaTime;
	}

	void OnCollisionEnter(Collision collisionInfo)
	{
		if(collisionInfo.gameObject.tag == "Wall"){
			next_destination.x = Mathf.Round(transform.position.x);
			next_destination.y = Mathf.Round(transform.position.y);
			transform.position = next_destination;
			move_failed = true;
			time = 0f;
		}
	}

	public void DisableMovement(){
		move_disabled = true;
	}
	public void EnableMovement(){
		move_disabled = false;
	}

	public PlayerScript.Direction GetDirection(){
		Vector3 dir = next_destination - last_position;
		dir = Vector3.Normalize(dir);
		if(Mathf.Round(dir.x) == -1){
			return PlayerScript.Direction.Left;
		}
		else if(Mathf.Round(dir.x) == 1){
			return PlayerScript.Direction.Right;
		}
		else if(Mathf.Round(dir.y) == 1){
			return PlayerScript.Direction.Up;
		}
		else if(Mathf.Round(dir.y) == -1){
			return PlayerScript.Direction.Down;
		}
		Debug.Log("NOOOOOO");
		return PlayerScript.Direction.Up;
	}
}
}