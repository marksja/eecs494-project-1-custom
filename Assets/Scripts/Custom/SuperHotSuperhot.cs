﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class SuperHotSuperhot : MonoBehaviour {

	[Range(0.1f, 1f)]
	public float timescale;

	void Update()
	{
		Time.timeScale = timescale;
	}
}
}