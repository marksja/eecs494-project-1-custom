﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ZeldaCustom {
public class Sword : MonoBehaviour {

	public float active_time;
	public GameObject projectilePrefab;

	PlayerScript player;
	public UnityEvent on_attack;
	public UnityEvent on_destroy;

	static GameObject currentProjectile;

	// Use this for initialization
	void Start() {
		StartCoroutine(/*Stop,*/WaitAMinute());
		player = transform.parent.GetComponent<PlayerScript>();
		//on_attack.AddListener(player.DisableMovement);
		//on_destroy.AddListener(player.EnableMovement);
		on_attack.Invoke();
		GameController.audioManager.Play("Sword_Slash");
	}

	void LaunchSword() {
		if(player.CanFireMagicSword() && currentProjectile == null){
			currentProjectile = Instantiate(projectilePrefab, this.transform.position, this.transform.rotation);
			currentProjectile.GetComponent<SwordProjectile>().direction = player.VectorFromDirection(player.facing_direction);
		}
		Destroy(gameObject);
		on_destroy.Invoke();
	}

	IEnumerator WaitAMinute() {
		yield return new WaitForSeconds(active_time);
		LaunchSword();
	}
}
}