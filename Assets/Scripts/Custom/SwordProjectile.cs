﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class SwordProjectile : MonoBehaviour
{
	public GameObject particleProjectilePrefab;
	public Sprite altSprite;
	public float speed;
	public Vector2 direction;

	SpriteRenderer sr;
	Sprite baseSprite;
	bool swap = true;

	void Start()
	{
		sr = GetComponent<SpriteRenderer>();
		baseSprite = sr.sprite;
		GameController.audioManager.Play("Sword_Shoot");
	}
	
	void Update()
	{
		transform.Translate(direction * speed * Time.deltaTime, Space.World);
		if (swap) {
			sr.sprite = altSprite;
			swap = false;
		}
		else {
			swap = true;
			sr.sprite = baseSprite;
		}
	}

	void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" || other.tag == "CameraBounds" || other.tag == "Old Guy") {
			Instantiate(particleProjectilePrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
}