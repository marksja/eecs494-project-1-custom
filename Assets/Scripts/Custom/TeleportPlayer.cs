﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class TeleportPlayer : MonoBehaviour
{
	public bool withFadeEffect = true;
	public bool playStairsSound = true;
	public Vector2 targetRoom;
	public Vector2 playerOffset;

	PlayerScript player;

	void Start()
	{
		player = GameObject.Find("Player").GetComponent<PlayerScript>();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {
			StartCoroutine(MovePlayer());
		}
	}

	IEnumerator MovePlayer()
	{
		// First move the player to the ceneter of this tile
		yield return player.MoveToPoint(transform.position);
		if (playStairsSound) {
			GameController.audioManager.Play("Stairs");
		}
		yield return Teleport();
	}

	// Execute the actual room transition
	IEnumerator Teleport()
	{
		PlayerScript playerScript = player.GetComponent<PlayerScript>();
		playerScript.DisableMovement();

		if (withFadeEffect) {
			yield return GameController.instance.FadeToBlack(1.0f);
		}

		JumpToRoom roomTransition = Camera.main.GetComponent<JumpToRoom>();
		Vector2 defaultPlayerOffset = roomTransition.playerOffset;
		roomTransition.playerOffset = playerOffset;
		roomTransition.room = targetRoom;
		roomTransition.GO = true;
		yield return new WaitForEndOfFrame();
		roomTransition.playerOffset = defaultPlayerOffset;
		roomTransition.room = new Vector2(0, 0);

		if (withFadeEffect) {
			yield return GameController.instance.FadeFromBlack(1.0f);
		}

		playerScript.EnableMovement();
	}
}
}