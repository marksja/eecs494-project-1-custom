﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZeldaCustom {
public class TimeScaleCheck : MonoBehaviour
{
	public ArrowKeyMovement arrowKeyMovement;
	
	void Update()
	{
		if (!arrowKeyMovement.ControlsEnabled) {
			Time.timeScale = 1;
		}
		else if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0 ||
				 (Input.GetButton("B") && GameController.instance.hasClock))
		{
			Time.timeScale = 1;
		}
		else {
			Time.timeScale = 0.01f;
			
		}
		Time.fixedDeltaTime = 0.02f * Time.timeScale;
	}
}
}