﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ZeldaCustom {
public class UIHearts : MonoBehaviour
{
	public const int numHeartUI_c = 16;

	public Sprite emptyHeartSprite;
	public Sprite halfHeartSprite;
	public Sprite fullHeartSprite;
	public GameObject heartUIPrefab;
	public Health playerHealth;

	private Image[] hearts;

	// Spawn all heart images
	void Start()
	{
		hearts = new Image[numHeartUI_c];
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 8; ++j) {
				GameObject heart = Instantiate(heartUIPrefab);
				heart.transform.SetParent(this.transform);
				heart.transform.localScale = Vector3.one;	// Scale is messed up on Instantiation for some reason
				heart.transform.localPosition = new Vector3(j * 8, i * 8, 0);
				heart.name = "heart" + ((i * 8) + j);

				Image im = heart.GetComponent<Image>();
				im.sprite = emptyHeartSprite;
				im.enabled = false;
				hearts[i*8 + j] = im;
			}
		}
	}

	void Update()
	{
		UpdateMaxHearts(playerHealth.health);
		UpdateHealth(playerHealth.current_health);
	}

	// Updates the UI to only show the given number of hearts
	public void UpdateMaxHearts(int maxHealth)
	{
		for (int i = 0; i < numHeartUI_c; i += 2) {
			hearts[i].enabled = (i < maxHealth);
		}
	}

	// Updates the UI to reflect the amount of health the player has
	public void UpdateHealth(int health)
	{
		int fullHearts = (int)(health / 2);
		bool halfHeart = (health % 2 > 0);
		for (int i = 0; i < numHeartUI_c; ++i) {
			if (i < fullHearts) {
				hearts[i].sprite = fullHeartSprite;
			}
			else if (i == fullHearts && halfHeart) {
				hearts[i].sprite = halfHeartSprite;
			}
			else {
				hearts[i].sprite = emptyHeartSprite;
			}
		}
	}
}
}