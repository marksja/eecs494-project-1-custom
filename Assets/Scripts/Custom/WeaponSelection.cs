﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ZeldaCustom {
public class WeaponSelection : MonoBehaviour
{
	[System.Serializable]
	public class WeaponData
	{
		public string nameForDebugOnly;
		public PlayerScript.Weapon weapon;
		public Sprite sprite;
	}

	public const int iconWidth_c = 24;
	public const int iconHeight_c = 24;

	public RectTransform weaponSelectionPanel;
	public GameObject weaponUIPrefab;
	[Tooltip("The sprite that is shown in the status menu representing the player's secondary weapon")]
    public Image weaponDisplayImage;
	public PlayerScript player;
	public List<WeaponData> weaponData;

	int selectionIdx = 0;
	float panelWidth;
	GameObject cursor;

	List<GameObject> liveUiPrefabs;

	void Start()
	{
		liveUiPrefabs = new List<GameObject>();
		cursor = weaponSelectionPanel.Find("Cursor").gameObject;
		InitSelectionDisplay();
	}
	
	// HACK: have to update AFTER player update so they don't swing the sword when we close this menu
	// Otherwise, when we press "A" to select an item, it also applies to the player and causes
	// them to swing their sword on the same frame, which is annoying.
	void LateUpdate()
	{
		bool displayed = CheckDisplay();
		if (!displayed) {
			return;
		}

		MoveCursor();
		CheckSelection();
	}

	public void InitSelectionDisplay()
	{
		foreach (GameObject obj in liveUiPrefabs) {
			Destroy(obj);
		}
		liveUiPrefabs = new List<GameObject>();
		panelWidth = player.availableWeapons.Count * iconWidth_c;
        weaponSelectionPanel.sizeDelta = new Vector2(panelWidth, iconHeight_c);
		cursor.transform.localPosition = new Vector3(0, 0, 0);
        for (int i = 0; i < player.availableWeapons.Count; ++i) {
			WeaponData wd = weaponData.Find(x => x.weapon == player.availableWeapons[i]);
            GameObject weapon = Instantiate(weaponUIPrefab);
            weapon.transform.SetParent(weaponSelectionPanel);
            weapon.transform.localScale = Vector3.one;	// Scale is messed up on Instantiation for some reason
            weapon.transform.localPosition = new Vector3(i * iconWidth_c, 0, 0);
            weapon.name = "UI" + wd.weapon;
            weapon.transform.Find("Image").GetComponent<Image>().sprite = wd.sprite;
			liveUiPrefabs.Add(weapon);
        }

        // Display player's initial weapon
        weaponDisplayImage.sprite = (weaponData.Find(x => x.weapon == player.currentWeapon)).sprite;
	}

	// Show or hide the panel based on input.
	// Returns true if the panel is currently displayed.
	bool CheckDisplay()
	{
		GameObject panel = weaponSelectionPanel.gameObject;
		// B button backs out of menu
		if (Input.GetButtonDown("B") && panel.activeSelf) {
			HidePanel();
		}
		// Start button toggles menu
		if (Input.GetButtonDown("Start") && !panel.activeSelf) {
			ShowPanel();
        }
		return panel.activeSelf;
	}

	// Move the cursor left and right along the weapon selection, wrapping around if
	// we go off the edge.
	void MoveCursor()
	{
		if (Input.GetButtonDown("Left")) {
			selectionIdx--;
			if (selectionIdx < 0) {
				selectionIdx = player.availableWeapons.Count - 1;
			}
		}
		else if (Input.GetButtonDown("Right")) {
			selectionIdx++;
			if (selectionIdx >= player.availableWeapons.Count) {
				selectionIdx = 0;
			}
		}

		cursor.transform.localPosition = new Vector3(selectionIdx * iconWidth_c, 0, 0);
	}

	// If the player presses the 'A' button while the menu is open, close the menu and
	// report the selected weapon to the player
	void CheckSelection()
	{
		if (Input.GetButtonDown("A")) {
			PlayerScript.Weapon weapon = player.availableWeapons[selectionIdx];
			player.currentWeapon = weapon;
            weaponDisplayImage.sprite = (weaponData.Find(x => x.weapon == player.currentWeapon)).sprite;
			HidePanel();
		}
	}

	// Pauses the game and enables the weapon selection panel
	void ShowPanel()
	{
		weaponSelectionPanel.gameObject.SetActive(true);
        cursor.transform.localPosition = new Vector3(selectionIdx * iconWidth_c, 0, 0);
		player.DisableMovement();
	}

	// Disables the weapon selection panel and unpauses the game
	void HidePanel()
	{
		weaponSelectionPanel.gameObject.SetActive(false);
		player.EnableMovement();
	}
}
}