EECS 494 Fall 2017 - Project 1
Cooper Riehl (cariehl) and Jacob Marks (marksja)

Credit goes to the following authors

Audio:
    Video Game Music Preservation Foundation (http://vgmpf.com/Wiki/index.php/The_Legend_of_Zelda_(NES))
    HelpTheWretched (http://noproblo.dayjo.org/ZeldaSounds/)

Sprites:
    Item and Enemy sprites from Deathbringer, Superjustinbros, Ville10 @ The Spriters Resource (https://www.spriters-resource.com/nes/legendofzelda/)
    UI sprites from videogamesprites.net (http://www.videogamesprites.net/Zelda1/Objects/)

Font:
    The FontStruction “The Legend of Zelda NES”
(https://fontstruct.com/fontstructions/show/818869) by “Gwellin” is licensed
under a Creative Commons Attribution Non-commercial Share Alike license
(http://creativecommons.org/licenses/by-nc-sa/3.0/).
